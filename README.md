# Spec
 - online SSO integration --> privelages given by admin (veronica)
 - different levels of access --> manager, analysts, admins(HR), admin(application)
 - managers and analysts have to have a min number of people ranked
 - tool should be scalable and built for long term use
 - algorithm needs to exist
 - export into excel
 - automatic emailing but admin decides when - notify managers and analyst of who they matched
 	- When making manual changes to excel, we would need a mechanism to upload the excel files or a mechanism to modify matches manually. The notifications would then send these matches.

# Algorithm
 - What happens in the case of non-matching sets?
 	- more managers than analysts 
    	- regular case --> match analysts until we run out of them
    	- more students than managers
    		- allow access for admin to assign multiple students to a manager manually
    		- do we throw an error and mandate more managers than analysts
    		- future case: allow for managers to take multiple analysts
    			- when matching do we make sure all managers have an analyst before giving someone more (non optimal) or do it by preference (optimal)
    
# SSO Integration
 - How do we get access to the SSO API? 
 - What kind of info do we need to pull from the SSO credentials of the users?

# Hosting environment
 - We don't budget for this application, in this case, where would we be hosting this app, that is both free and internal/secure

# Database
 - do we want to be storing the results somewhere for future reference
 	- in email it said that admin should be able to access consolidated rankings at any time --> should these also be stored

# Emails
 - what account is sending the emails --> Admin (application) or from HR (veronica) 
 - do not reply email address specific to the app?

# Data Input
 - How would you like the manager and analyst to input their ranking? In built UI? Some other way?
 - Would you be putting in the available list of managers and analysts that are available for matching? Another UI Screen?
 - When users input ranking are they modifiable until algorithm is run?

# Data Export
 - What format is needed, what columns/rows/data would you like to see? Base Schema: Manager | Analyst
 - What format should the output be in? Excel? would you like support for more formats?

# UI 
 - do we have access to the Citi angular UI initiative (Elemental)?
 - different views for different users?

# Other Questions
 - If the hope is to expand to other programs/locations, how do they do their matching? Is it significantly different than ours?